type FeatureFlag = "roll-app" | "npc-generator";

const featureFlags: Map<FeatureFlag, boolean> = new Map([
  ["roll-app", false],
  ["npc-generator", false],
]);

export default featureFlags;
