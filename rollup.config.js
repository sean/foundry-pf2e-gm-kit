import nodeResolve from "@rollup/plugin-node-resolve";
import path from "path";
import copy from "rollup-plugin-copy";
import del from "rollup-plugin-delete";
import swc from "rollup-plugin-swc";

const outputDir = path.resolve(process.env.OUTPUT_DIR || "build");

export default {
  input: ["src/main.ts"],
  output: {
    dir: outputDir,
    sourcemap: true,
    format: "es",
  },
  plugins: [
    nodeResolve({
      extensions: [".js", ".ts"],
    }),
    del({
      targets: outputDir,
    }),
    copy({
      targets: [
        { src: "styles", dest: outputDir },
        { src: "module.json", dest: outputDir },
        { src: "templates", dest: outputDir },
        { src: "lang", dest: outputDir },
      ],
    }),
    swc({
      sourceMaps: true,
    }),
  ],
};
